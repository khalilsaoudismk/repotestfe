import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InscrirEntrepriseComponent } from './inscrir-entreprise.component';

describe('InscrirEntrepriseComponent', () => {
  let component: InscrirEntrepriseComponent;
  let fixture: ComponentFixture<InscrirEntrepriseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InscrirEntrepriseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InscrirEntrepriseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
