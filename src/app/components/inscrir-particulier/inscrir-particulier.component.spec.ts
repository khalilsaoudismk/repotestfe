import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InscrirParticulierComponent } from './inscrir-particulier.component';

describe('InscrirParticulierComponent', () => {
  let component: InscrirParticulierComponent;
  let fixture: ComponentFixture<InscrirParticulierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InscrirParticulierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InscrirParticulierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
