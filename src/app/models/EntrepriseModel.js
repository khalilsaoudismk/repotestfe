export class Entreprise {
  registreDeCommerce: string;
  nomEntreprise: string;
  nom: string;
  prenom: string;
  telephone1: string;
  telephone2: string;
  email: string;
  adresse: string;
  ville: string;
  pays: string;
}
