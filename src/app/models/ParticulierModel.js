export class Particulier {
  adresseEmail : string;
  password: string;
  nom: string;
  prenom: string;
  dateNaissance: Date;
  adresse: string;
  ville: string;
  pays: string;
  niveau: string;
  profile: string;
  telephone: string;
}
