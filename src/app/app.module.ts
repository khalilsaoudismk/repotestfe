import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ParticulierService } from 'src/app/service/particulierService';
import { AppComponent } from './app.component';
import { InscrirParticulierComponent } from './components/inscrir-particulier/inscrir-particulier.component';
import { InscrirEntrepriseComponent } from './components/inscrir-entreprise/inscrir-entreprise.component';
import { LoginComponent } from './components/login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";
import {HttpModule} from '@angular/http';


const appRoutes:Routes=[
  { path: 'login-component', component: LoginComponent },
  { path: 'InscrirParticulierComponent', component: InscrirParticulierComponent },
  { path: 'InscrirEntrepriseComponent', component: InscrirEntrepriseComponent },
  // {path:'',redirectTo:'/AppComponent',pathMatch:'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    InscrirParticulierComponent,
    InscrirEntrepriseComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule, RouterModule, RouterModule.forRoot(appRoutes), HttpModule, FormsModule, ReactiveFormsModule
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

